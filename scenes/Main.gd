extends Node

export var pickup : PackedScene
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	var locations = [[250, 50], [50, 250], [450, 250], [250, 450]]
	
	for location in locations:
		var new_pickup = pickup.instance()
		new_pickup.position.x = location[0]
		new_pickup.position.y = location[1]
		add_child(new_pickup)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Player_hit():
	score += 1
	$scoreboard.text = str(score)